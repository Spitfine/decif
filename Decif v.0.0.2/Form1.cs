﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Decif_v._0._0._2
{
    public partial class Decif : Form
    {
        public Decif()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
            
            // excepções
        {
            if (txtVdia.Text == "")
            {
                MessageBox.Show("Preencher valor dia");
            }
            else if (txtVdia.Text == "0")
            {
                MessageBox.Show("O valor por dia 0 não é válido");
            }
            else if (txtDcompletos.Text == "")
            {
                MessageBox.Show("Indicar número de dias completos");
            }
            else if (txtHoras.Text == "")
            {
                MessageBox.Show("Indicar número de horas");
            }
            else
            {

                // variaveis

                double Vdia, Dcompletos, Horas;
                double Vreceber;

                Vdia = Convert.ToDouble(txtVdia.Text);
                Dcompletos = Convert.ToDouble(txtDcompletos.Text);
                Horas = Convert.ToDouble(txtHoras.Text);


                // calculos
                Vreceber = Vdia * Dcompletos + (Vdia / 24 * Horas);

                // resultado

                txtVreceber.Text = Vreceber.ToString("c");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach(Control c in Controls)
            {
                if (c is TextBox)
                {
                    c.Text = "";
                }
            }
        }
    }
}
